﻿USE practica6;

-- Consulta 1

  -- c1
  SELECT * FROM tema t 
    WHERE t.descripcion='Bases de Datos';

  -- c2
  SELECT * FROM articulo a 
    WHERE a.anio=1990;

  -- final
  SELECT c2.titulo_art 
    FROM (
      SELECT * FROM tema t 
        WHERE t.descripcion='Bases de Datos'
      )c1 
    JOIN (
      SELECT * FROM articulo a 
        WHERE a.anio=1990
      )c2 
    USING (codtema);

-- Consulta 2
  
  SELECT r.titulo_rev 
    FROM revista r
      JOIN (
        SELECT c2.referencia 
          FROM (
            SELECT a.referencia, a.codtema 
              FROM articulo a
          )c2
        GROUP BY c2.referencia
        HAVING COUNT(*)=(
          SELECT COUNT(*) 
            FROM tema t 
          )
        )c1 
      USING (referencia); 

-- Consulta 3

  -- c1
  SELECT * FROM revista r 
    JOIN (
      SELECT r.referencia 
        FROM revista r
    )c1 
    USING (referencia);

  -- c2
  SELECT a.referencia FROM articulo a 
    JOIN (
      SELECT * FROM tema t 
        WHERE t.descripcion <> 'Medicina'
    )c1 
    USING (codtema);

  -- final
  SELECT c2.titulo_rev FROM (
    SELECT * FROM revista r 
      JOIN (
        SELECT r.referencia 
          FROM revista r
      )c1 
      USING (referencia)
    )c2 
    LEFT JOIN (
      SELECT a.referencia 
        FROM articulo a 
          JOIN (
            SELECT * FROM tema t 
              WHERE t.descripcion <> 'Medicina'
          )c1 
        USING (codtema)
    )c3 
    USING (referencia)
    WHERE c3.referencia IS NULL;

-- Consulta 4

  -- c1
  SELECT c1.dni FROM (
    SELECT * FROM articulo a WHERE a.anio=1991
  )c1 
  JOIN (
    SELECT * FROM tema t WHERE t.descripcion='SQL'
  )c2
  USING (codtema);

  -- c2
  SELECT c1.dni FROM (
    SELECT * FROM articulo a WHERE a.anio=1992
  )c1 
  JOIN (
    SELECT * FROM tema t WHERE t.descripcion='SQL'
  )c2
  USING (codtema);

  -- final
  SELECT a.nombre 
    FROM (
      SELECT c1.dni 
        FROM (
          SELECT * FROM articulo a 
            WHERE a.anio=1991
        )c1 
      JOIN (
        SELECT * FROM tema t 
          WHERE t.descripcion='SQL'
      )c2
      USING (codtema)
    )c1 
    JOIN (
      SELECT c1.dni 
        FROM (
          SELECT * FROM articulo a 
            WHERE a.anio=1992
        )c1 
        JOIN (
          SELECT * FROM tema t 
            WHERE t.descripcion='SQL'
        )c2
        USING (codtema)
      )c2
      USING (dni)
    JOIN autor a
    USING (dni);

  -- Consulta 5

    -- c1 
    SELECT a.titulo_art 
      FROM articulo a 
        WHERE a.anio=1993;

    -- c2
    SELECT * FROM autor a 
      WHERE a.universidad='Politecnica de Madrid';

    -- final
    SELECT c1.titulo_art 
      FROM (
        SELECT * FROM articulo a 
          WHERE a.anio=1993
      )c1 
      JOIN (
        SELECT * FROM autor a 
          WHERE a.universidad='Politecnica de Madrid'
      )c2 
      USING (dni);